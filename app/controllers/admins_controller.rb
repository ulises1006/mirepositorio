class AdminsController < ApplicationController
  #before_action :set_zombie, only: [:show, :edit, :update, :destroy]

  # GET /zombies
  # GET /zombies.json
  def index
    @users = User.all
    @rotten_zombies = Zombie.where(rotten: true)
    
  end
  def new
    @user = User.new
  end

   def edit
  end
  
  def show
  end


 

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to user_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :role)
    end
end
